﻿Задание №1
Какой тип будет у данного выражения:
typeof 1 // "number"
typeof “1” // "string"
typeof NaN  // "number"
typeof true // "boolean"  
typeof {} // "object"
typeof 1 && 3 // "number"
typeof (1 && 3) // "number"
typeof undefined // "undefined"
typeof null // "object"
typeof 5 || 5 // "number"


Задание №2
Примеры (определи, что получится):
"1234".substring(0, 2) // "12"
"1234".substr(0, 2) // "12"

Задание №3
Верно ли утверждение:
"1234".substr(2, 3) === "1234".substring(2, 3) //  34 != 3, false
"1234".substring(0, 2) === "1234".slice(0, 2) // 12 === 12, true
"1234".substr(0, 2) === "1234".indexOf(0, 2) // 12 != -1, false